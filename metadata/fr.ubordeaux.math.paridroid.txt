Categories:Science & Education
License:GPLv3+
Web Site:http://pari.math.u-bordeaux.fr/paridroid/
Source Code:http://pari.math.u-bordeaux.fr/cgi-bin/gitweb.cgi?p=paridroid.git;a=summary
Issue Tracker:http://pari.math.u-bordeaux.fr/Bugs/

Auto Name:PariDroid
Summary:PARI/GP computer algebra system
Description:
PariDroid is a port of PARI/GP to Android.

PARI/GP is a widely used computer algebra system designed for fast
computations in number theory (factorizations, algebraic number theory,
elliptic curves...), but also contains a large number of other useful
functions to compute with mathematical entities such as matrices,
polynomials, power series, algebraic numbers etc., and a lot of
transcendental functions.
.

Repo Type:git
Repo:http://pari.math.u-bordeaux.fr/git/paridroid.git

Build:2.7.2.1.3,8
    commit=2.7.2.1.3
    subdir=PariDroid
    srclibs=pari@pari-2.7.2
    target=android-10
    build=ANDROID_NDK=$$NDK$$ && export ANDROID_NDK && cd .. && \
        make pari
    buildjni=yes
    ndk=r10d

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.7.2.1.3
Current Version Code:8

