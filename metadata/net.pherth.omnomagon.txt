Categories:Office
License:NewBSD
Web Site:http://www.omnomagon.de/
Source Code:https://github.com/vIiRuS/Omnomagon
Issue Tracker:https://github.com/vIiRuS/Omnomagon/issues
FlattrID:538996

Auto Name:Omnomagon
Summary:Cafeteria info
Description:
Omnomagon shows you the current Menue of your university cafeteria -
including useful information for allergy sufferers, vegetarians and
vegans.

Currently supported cafeterias:
- Berlin
- Ulm
.

Repo Type:git
Repo:https://github.com/vIiRuS/Omnomagon.git

Build:0.9.6,3
    disable=can't be published see issue tracker (at HEAD)
    commit=HEAD
    srclibs=ActionBarSherlock@4.2.0,ViewPagerIndicator@65457e,Amazing-ListView@4
    extlibs=jsoup/jsoup-1.6.3.jar
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i 's@\(android.library.reference.2=\).*@\1$$ViewPagerIndicator$$@' project.properties && \
        sed -i 's@\(android.library.reference.3=\).*@\1$$Amazing-ListView$$@' project.properties

Build:0.10.0,4
    commit=1.0
    extlibs=android/android-support-v4.jar
    srclibs=JSoup@jsoup-1.8.1.a,1:PagerSlidingTabStrip@v1.0.1,2:Support/v7/appcompat@android-5.0.1_r1,Support/v7/recyclerview@android-5.0.1_r1
    prebuild=mkdir -p $$Support$$/libs && cp libs/android-support-v4.jar $$Support$$/libs && \
        echo -e "java.source=7\njava.target=7" >> $$Support$$/local.properties && \
        cp -fR $$Support$$/../recyclerview/src src/ && \
        mkdir -p $$PagerSlidingTabStrip$$/libs && \
        mv libs/android-support-v4.jar $$PagerSlidingTabStrip$$/libs
    build=pushd $$JSoup$$ && $$MVN3$$ package && popd && \
        cp $$JSoup$$/target/jsoup-1.8.1.jar libs/
    target=android-21

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.10.0
Current Version Code:4

